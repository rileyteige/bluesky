const flightLog = require('./api/flightLog');
const airport   = require('./api/airport');
const aircraft  = require('./api/aircraft');

module.exports = {
	registerRoutes: function (app) {
		app.use('/api/flightLog', flightLog);
		app.use('/api/airport', airport);
		app.use('/api/aircraft', aircraft);
	}
}