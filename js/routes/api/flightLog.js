'use strict';

const router = require('express').Router();
const flightLogService = require('services/flightLog');

router.get('/', (req, res) => {
	flightLogService.getFlightLog().then(log => res.send(log));
});

router.post('/', (req, res) => {
	if (!req.body) return res.sendStatus(400);
	flightLogService.addEntry(req.body).then(id => res.send('' + id));
})

module.exports = router;