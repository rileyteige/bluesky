'use strict';

const router = require('express').Router();
const airportService = require('services/airport');

router.get('/', (req, res) => {
	airportService.getAirports().then(airports => res.send(airports));
});

router.get('/:icao', (req, res) => {
	var icao = '' + req.params.icao;
	airportService.search(icao).then(airports => res.send(airports));
});

module.exports = router;