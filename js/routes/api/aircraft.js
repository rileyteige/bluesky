'use strict';

const router = require('express').Router();
const aircraftService = require('services/aircraft');

router.get('/', (req, res) => {
	return aircraftService.getAircraft().then(aircraft => res.send(aircraft));
});

module.exports = router;